# Solarus GUI-specific -D options.

# Disable GUI native dialogs.
set(SOLARUS_GUI_NO_NATIVE_DIALOGS "OFF" CACHE BOOL "Disable native dialogs in the Solarus GUI.")
