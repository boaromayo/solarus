---@class sol
local m = {}

---@type sol.text_surface
m.text_surface = nil

---@type sol.language
m.language = nil

---@type sol.timer
m.timer = nil

---@type sol.main
m.main = nil

---@type sol.state
m.state = nil

---@type sol.movement
m.movement = nil

---@type sol.game
m.game = nil

---@type sol.video
m.video = nil

---@type sol.menu
m.menu = nil

---@type sol.surface
m.surface = nil

---@type sol.shader
m.shader = nil

---@type sol.file
m.file = nil

---@type sol.input
m.input = nil

---@type sol.sprite
m.sprite = nil

---@type sol.audio
m.audio = nil

_G.sol = m

return m